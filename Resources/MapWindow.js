function ApplicationWindow(title) {
	var win = Ti.UI.createWindow({
		title: title,
		backgroundColor: 'white'
	});

	//en array för att lagra kartnålar som vi skapar längre ner
	var mapPins = [];

	//en array för att spara all gps-data vi får från backend
	var gpsData = [];

	//XXX vi läser in kart-modulen, som vi först måste lägga till i tiapp.xml
	var mapModule = require('ti.map');

	//XXX via kart-modulen skapar vi en kartvy
	var mapView = mapModule.createView({
		//XXX visa användarens pos på kartan, för iOS, måste man lägga in en nyckel i tiapp.xml:
		/*
			<key>NSLocationWhenInUseUsageDescription</key>
			<string>
    	        Show your location on the map.
    	        </string>
		*/
		//se tiapp.xml för att se exakt var
		userLocation: true,
		//XXX ställ in vilken typ av karta vi ska visa
		mapType: mapModule.NORMAL_TYPE, //HYBRID_TYPE //SATELLITE_TYPE
		//XXX sätter en start pos och inzoomning för kartan
		region: {
			latitude: 55.6005,
			longitude: 13.10324,
			latitudeDelta: 0.02,
			longitudeDelta: 0.02
		},
		//XXX ändrar färg på användarens position och övriga färger i kartan
		tintColor: '#ff0000',
		//XXX visar kartans skala uppe i vänstra härnet
		showsScale: true,
		//XXX raden under förhindrar att användaren kan dra och zooma i kartan (den blir helt statisk)
		// touchEnabled: false,
		//XXX det går att placera kartan med: top, right, left, bottom
		// top: 0,
		//XXX det går även att begränsa kartans storlek (bredd, höjd)
		// height: '50%'
	});
	win.add(mapView);

	mapView.addEventListener('click', function(e){
		logg('click on map e =', e, true);

		//XXX när vi trycker på en knapp som ligger till höger (leftButton för vänsterknappen)
		// i popupen för kartnålen
		if (e.clicksource == 'rightButton'){

			//XXX vi läser in ett fönster
			var AppWin = require('ApplicationWindow');

			//XXX vi skapar ett fönster och skickar med en titel och gps-datan för den aktuella kartnålen
			var newWin = AppWin('Detail', gpsData[e.annotation.arrayId]);

			// logg('arrayId = ', e.annotation.arrayId);
			// logg(gpsData[e.annotation.arrayId]);

			//XXX vi öppnar fönstret via tabGroupen så vi får en snygg animering och en bakåt-kanpp
			// vill vi öppna fönstret i helskärm, så kan vi istället köra:
			// newWin.open()
			win.containingTab.open(newWin);
		}
	});

	//XXX en view för att placera i en kartnåls-popup eller call-out, när man trycker på en nål
	var leftView = Ti.UI.createView({
		width: 90,
		height: 35,
		backgroundColor: '#ff0000'
	});

	//XXX vi skapar en kartnål, med en pos och title och undertitle
	//		den får även en view till vänster och en knapp till höger
	var mapPin = mapModule.createAnnotation({
		latitude: 55.60051,
		longitude: 13.00324,
		title: 'Test',
		subtitle: 'Adress',
		leftView: leftView,
		rightButton: '/images/KS_nav_ui.png'
		// leftButton: Titanium.UI.iPhone.SystemButton.INFO_LIGHT
	});
	//XXX man lägger till nålar på kartan med addAnnotation (en nål) eller addAnnotations (en array med flera)
	mapView.addAnnotation(mapPin);


	//XXX funktion som tar emot gps-positioner
	function gotPosition(e) {
		//XXX eventet tar emot gps-data (se loggen för mer detaljer)
		Ti.API.info('gotPosition(), e ', e);

		//XXX e.code innehåller en ev felkod, 0 = ingen felkod
		//	e.success är antingen 1 (true) eller 0 (false)
		if (e.code == 0 && e.success == true) {
			//XXX e.coords innehåller gps-datan, tex: hastighet, höjd, lat, lon, m.m
			if (e.coords) {
				//XXX vi hämtar ut pos ur gps-datan
				var lat = e.coords.latitude;
				var lon = e.coords.longitude;

				//XXX vi uppdaterar kartans position så den centrerar på användarens pos
				mapView.setRegion({
					latitude: lat,
					longitude: lon,
					//XXX vi vill inte ändra på zoom-nivån, så vi sätter samma som kartan redan har
					latitudeDelta: mapView.region.latitudeDelta,
					longitudeDelta: mapView.region.longitudeDelta
				});
			}
		}
	};

	//XXX accuraccy och distanceFilter, är iOS-specifika
	if (OS_is_iOS) {
		//XXX sätter noggrannheten till ca 10 meter,
		//	sätt denna för att spara på batteri ifall noggrannheten inte behöver vara max
		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_NEAREST_TEN_METERS;

		//XXX sätter hur ofta vi vill få en uppdatering
		//	dvs användaren måste flytta sig minst 50 meter innan vi får en ny pos
		//	detta verkar dock inte funka i simulatorn, kan vara en bugg i Ti SDK:t
		Ti.Geolocation.distanceFilter = 50;
 	}

 	//XXX ber att få en position av enheten, när vi får en pos så körs callback-funktionen: gotPosition
	Ti.Geolocation.getCurrentPosition(gotPosition);

	//XXX om vi sätter en lyssnare på eventet 'location', så vår vi positoner så fort användaren
	//		flyttat på sig, dessa kan även köras när appen är i bakgrunden
	// Ti.Geolocation.addEventListener('location', gotPosition);

	//XXX vill man stoppa uppdateringarna kör man:
	// Ti.Geolocation.removeEventListener('location', gotPosition);

	//XXX vi lyssnar på ett globalt event som heter 'gotGpsData',
	//		det blir globalt då vi använder: Ti.App
	Ti.App.addEventListener('gotGpsData', function(e) {
		logg('gotGpsData = ', e.gpsData);

		//XXX vi sparar all gps-data i vår globala variabel, så vi kan komma åt den senare
		gpsData = e.gpsData;

		//XXX for-lopen använder vi för att skapa en kartnål för varje resturang
		for (var i = 0; i < e.gpsData.length; i++){
			//XXX vi sparar kartnålen tillfälligt i 'pin'
			var pin = mapModule.createAnnotation({
				latitude: e.gpsData[i].latitude,
				longitude: e.gpsData[i].longitude,
				title: e.gpsData[i].adress,
				subtitle: e.gpsData[i].phone,
				rightButton: '/images/KS_nav_ui.png',
				//XXX arrayId är en egen property vi hittat på, för att veta var i arrayen den finns
				arrayId: i
			});

			//XXX lägg till kartnålen i arrayen
			mapPins.push(pin);

		} // end for-loop

		//XXX när alla pins är skapade, lägg till de till kartan
		mapView.addAnnotations(mapPins);
	});


	return win;
};

module.exports = ApplicationWindow;
