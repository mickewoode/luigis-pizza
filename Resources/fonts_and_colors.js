/**
 **
 ** innehåller fördefinierade fonter och färger
 **
 **/

/** 		GRADIENTS
 **/
//formatter:off
var gradients = {
	topLine : {
		type: 'linear',
		startPoint: { x: '0%', y: '0%' 	 },
		endPoint: 	{ x: '0%', y: '100%' },
		colors: [
			{ color: '#aaaaaa', offset: 0.0 },
			{ color: '#9a9a9a', offset: 1.0 }
		]
	}
};
exports.gradients = gradients;
//formatter:on


/**		COLORS
 **/
var colors = {
	// background colors
	winBg: '#f9f5ed',


	// general colors
	defTint: '#0099b8',
	lightGray: '#cccccc',
	mediumGray: '#a6a6a6',
	gray: '#808080',
	darkGray: '#555555',
	black: '#000000',
	white: '#f5f5f5',
	red: '#f00',


	// for border & lines
	twBorder: '#bdbdbd',
	twRowBg: '#f3efec',
	activeBorder: '#399de1',
	dividerLine: '#26'+'ffffff', //XXX OBS! Denna används av addDividerLine och addVerticalDividerLine i app_functions
	dividerLineDark: '#52'+'000000' //XXX OBS! Denna används av addDividerLine och addVerticalDividerLine i app_functions

};
exports.colors = colors;


/**		FONTS
 **/
if (Ti.Platform.osname == 'android') {
	//Android, fontens filnamn (utan .ext)
	var fontFamilyNormal = 'Georgia',
	    fontFamilyItalic = 'Georgia Italic',
	    fontFamilyBold = 'Georgia Bold',
	    fontFamilyPizza = 'panpizza';

} else {
	//iOS, vad fonten heter "PostScript"-namnet
	var fontFamilyNormal = 'Georgia',
	    fontFamilyItalic = 'Georgia-Italic',
	    fontFamilyBold = 'Georgia-Bold',
	    fontFamilyPizza = 'PanPizza';
}
//formatter:off
var fonts = [],
	fontSize;

// ändra start och end värdena för att få ut fler storlekar
for (var i = 10; i <= 24; i++) {
	fontSize = i;

	fonts[i] = [];
	fonts[i]['normal'] = 	{	fontFamily: fontFamilyNormal,		fontSize: fontSize+'sp',			fontWeight: 'normal'		};
	fonts[i]['italic'] = 	{	fontFamily: fontFamilyItalic,		fontSize: fontSize+'sp',			fontWeight: 'italic'		};
	fonts[i]['bold'] =		{	fontFamily: fontFamilyBold,			fontSize: fontSize+'sp',			fontWeight: 'bold'		};
	fonts[i]['pizza'] =		{	fontFamily: fontFamilyPizza,			fontSize: (3*fontSize)+'sp',		fontWeight: 'normal'		};
}
exports.fonts = fonts;
//formatter:on


