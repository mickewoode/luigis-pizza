function ApplicationWindow(title) {

	var httpFuncs = require('http_funcs');

	var resturantData;

	//XXX denna funktion tar emot svaret från http-anropet vi gör längst ner i koden
	function gotData(state, result) {
		logg('gotData()...');
		/* XXX
		 * 	funktionen har två parametrar:
		 * 	- state (kan vara 'done' eller 'error')
		 * 	- result,
		 * 		innehåller datan (om det gick bra)
		 * 		eller felmeddelande och felkod
		 */
		if (state == 'done') {
			//XXX vi sparar datan från anropet så vi kan komam åt den igen senare
			resturantData = result;

			//XXX vi skickar iväg ett eget event, det blir globalt då Ti.App är globalt i hela appen
			// 		vi skapar en egen property: gpsData som vi tilldelar vår gps-data, vi precis fick
			// flera fönster kan lyssna på detta event
			Ti.App.fireEvent('gotGpsData', {
				gpsData: result
			});

			//XXX här skapas vi våra labels för öppettider längst ner
			makeOpenHoursLbls(result[0]);
		} else {
			//här gick det fel, ev meddela användaren
		}

		updateOpenClosedLbl();

		//XXX vi gömmer "snurrgrejen" ("loading" eller activityIndicatorn)
		activityIndicator.hide();
	}

	//XXX en funktion för att skapa/ändra ÖPPET el STÄNGT texten
	function updateOpenClosedLbl() {

		//XXX just nu är det alltid öppet, vi har inte byggt in stängt funtkionen än
		var openClosedTxt = L("open");

		//XXX vi formatterar en text med vårt ÖPPET eller STÄNGT värde
		var fullTxt = String.format(L('we_are'), openClosedTxt);
		//'Vi har '+openClosedTxt+' nu!';

		//XXX txtRange används av AttributedString nedanför här
		//		den sätter startpos och längd på den text som ska formatteras
		var txtRange = [fullTxt.indexOf(openClosedTxt), openClosedTxt.length];

		//XXX här skapas ett AttributedString objekt,
		//		baserat på vår fullTxt och med några attribut (färg och font)
		var attr = Ti.UI.createAttributedString({
			text: fullTxt,
			attributes: [{
				type: Ti.UI.ATTRIBUTE_FOREGROUND_COLOR,
				value: '#0f0',
				range: txtRange
			}, {
				type: Ti.UI.ATTRIBUTE_FONT,
				value: fc.fonts[14]['pizza'],
				range: txtRange
			}]
		});

		//XXX här applicerar vi strängen med formatteringen till vår label
		weAreOpenClosedLbl.attributedString = attr;
	}

	//XXX funktionen skapar våra labels med öppettider längst ner
	//
	function makeOpenHoursLbls(data) {
		logg('makeOpenHoursLbls(), data = ', data);

		//XXX vi har tre olika öppettider (må-fr, lö & sö)
		for (var i = 0; i < 3; i ++) {
			//vi skapar en sträng som motsvarar nyckeln i vår JSON, tex: 'open1'
			var key = 'open' + (i + 1);

			var lblHolder = Ti.UI.createView({
				top: 0,
				left: 0,
				right: 0,
				height: 45
			});

			//XXX vi har definerat 'open1', 'open2' & 'open3' i strings.xml i mappen 'i18n'
			//		appen "översätter" alltså open1 till "Mån-Fri"
			var weekDayLbl = Ti.UI.createLabel({
				text: L(key),
				textAlign: 'center',
				color: fc.colors.black,
				font: fc.fonts[22]['normal'],
				left: 15
			});
			lblHolder.add(weekDayLbl);

			var openHoursLbl = Ti.UI.createLabel({
				text: data[key], //XXX man kan hämta ut properties genom detta syntax: data["open1"]
				textAlign: 'center',
				color: fc.colors.black,
				font: fc.fonts[22]['normal'],
				right: 15
			});
			lblHolder.add(openHoursLbl);

			winHolder.add(lblHolder);
		}
	}

	var win = Ti.UI.createWindow({
		title: title,
		backgroundColor: bgColor
	});

	/* XXX
	* 	Vi skapar en hållare för vår 'vertical' layout
	* 	här i lägger vi allt som ska justera sig under varandra
	*/
	//XXX här har vi gjort om View:n till en ScrollView
	var winHolder = Ti.UI.createScrollView({
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		layout: 'vertical',
		//XXX de fyra properties här under är specifika för scrollView
		// showHorizontalScrollIndicator : true,
		showVerticalScrollIndicator: true,
		//XXX vi kan bestämma hur stort området som ska kunna scrollas ska vara
		// contentHeight: 3000,
		// contentWidth: 3000,
	});
	// devColor(winHolder);
	win.add(winHolder);

	var logoImageView = Ti.UI.createImageView({
		image: 'http://www.woodesoft.com/utbildning/luigis_pizza.png',//'/images/luigi.png',
		top: 8,
		width: 100,
		height: 100
	});
	winHolder.add(logoImageView);

	var welcomeLbl = Ti.UI.createLabel({
		/* XXX
		 'text' får en översatt sträng från strings.xml-filerna,
		 appen söker igenom xml-filen efter 'welcome' hittar den ingen översatt sträng,
		 skriver den ut det vi skrev i koden istället

		 dessa xml-filer ligger i mappen "i18n",
		 en mapp för varje språk:
		 en = English
		 sv = Svenska

		 se länk för fler koder:
		 https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes

		 koderna (två bokstäver långa) finns i kolumnen "639-1"
		 */
		text: ' ' + L('welcome') + ' ',
		/* XXX här hämtar vi font, från 'fonts_and_colors'-modulen
		 * 	vi väljer storlek 22 och typsnittet 'pizza' som vi fördefinerat
		 */
		font: fc.fonts[22]['pizza'],
		/* XXX
		 * 	färgen hämtas även den från 'fonts_and_colors'-modulen
		 */
		color: fc.colors.black
	});
	winHolder.add(welcomeLbl);

	var infoLbl = Ti.UI.createLabel({
		text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\nRow 2',
		textAlign: 'center',
		color: fc.colors.black,
		font: fc.fonts[18]['normal'],
		top: 0,
		/* XXX Om vi vill ha en marginal för vi antingen sätta left och right eller en width
		 */
		width: '90%',
		// left: 15,
		// right: 15
	});
	// devColor(infoLbl);
	winHolder.add(infoLbl);

	//XXX vi sätter ingen text i denna label än, detta sköts av updateOpenClosedLbl()
	var weAreOpenClosedLbl = Ti.UI.createLabel({
		// text: 'Vi har '+openClosedTxt+' nu!',
		textAlign: 'center',
		font: fc.fonts[22]['normal'],
		top: 16,
		width: '90%'
	});
	winHolder.add(weAreOpenClosedLbl);


	//XXX vi skapar en "snurrgrej" ("loading" eller activityIndicator)
	//		för att visa att hämtningen av datan ev kan lite ta tid
	var activityIndicator = Ti.UI.createActivityIndicator({
		color: '#f0f',
		font: {
			fontFamily: 'Helvetica Neue',
			fontSize: 22
		},
		message: 'Loading...',
		//XXX färgen under har angivits med ett alphavärde för genomskinlighet (cc)
		//
		backgroundColor: '#ccffffff',
		indicatorColor: '#f0f',
		style: Ti.UI.ActivityIndicatorStyle.DARK,
		top: 0,
		left: 0,
		height: '100%',//Ti.UI.SIZE,
		width: '100%'//Ti.UI.SIZE
	});
	win.add(activityIndicator);

	//XXX vi visar "snurrgrejen" ("loading" eller activityIndicatorn)
	activityIndicator.show();

	//XXX här fejkar vi lite... att hämtningen av datan tar 2.5 sekund
	//		bara så att vi hinner se "snurran"
	setTimeout(function() {

		//XXX vi påbörjar hämtning av datan från json-fil på server
		// httpFuncs.loadJSON('http://www.woodesoft.com/utbildning/pizzerior.json', gotData);

		//XXX vi påbörjar hämtning av datan från xml-fil på server
		httpFuncs.loadXML('http://www.woodesoft.com/utbildning/pizzerior.xml', gotData);

	}, 2500);//tiden är satt i ms, dvs 2.5 sekunder

	return win;
};

module.exports = ApplicationWindow;
