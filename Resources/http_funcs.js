/**
 Loads a json from a link

  	- parameter url: a http link to a file on a remote server

  	- parameter callback: the data will be passed to the callback on success or a error state will be returned
 */
function loadJSON(url, callback) {
	logg('loadJSON()...');

	var xhr = Ti.Network.createHTTPClient();
	xhr.setTimeout(45000);

	xhr.onload = function() {
		// logg('loadJSON(), onload >> responseText = ' + this.responseText);

		var json = JSON.parse(this.responseText);

		//check if a callback was submitted
		if (callback) {
			callback('done', json);
		}
	};

	xhr.onerror = function(err) {
		logg('loadJSON(), onerror >>', err);

		// alert(L('onerror') + ' ' + L('connection_error'));

		//check if a callback was submitted
		if (callback) {
			callback('error', err);
		}
	};

	logg('loadJSON(),  url   >> ' + url);

	xhr.open('GET', url);

	xhr.send();
}
exports.loadJSON = loadJSON;

/**
 * Loads xml from the specified url
 *
 * - parameter url: a http link to a file on a remote server
 * - parameter callback: the data will be passed to the callback on success or an error state will be returned
 */
function loadXML(url, callback) {
	//skapar en http-client så vi kan skapa anrop från den
	var xhr = Ti.Network.createHTTPClient();
	xhr.setTimeout(45000);

	var xmlData = [];

	//här deklarerar vi en funktion som ska utföras ifall vi fick kontakt och data
	xhr.onload = function(e) {
		//variabeln e kan innehålla "success"
		if (e.success && e.code == 0) {
			//vi fick kontakt (uppkoppling = ok) och vi har fått data
			try {
				var doc = this.responseXML.documentElement;

				var restaurants = doc.getElementsByTagName('restaurants');

				var pizzerias = restaurants.item(0).getElementsByTagName('pizzeria');

				logg('pizzerias.length = '+pizzerias.length);

				var adress,
				    longitude,
				    latitude,
				    phone,
				    one_item,
				    open_items;

				for (var i = 0; i < pizzerias.length; i++) {
					// xmlData[i] = [];

					one_item = pizzerias.item(i);


					adress = one_item.getElementsByTagName('adress').item(0).textContent;
					logg('adress = '+adress+ ', i = '+i);

					lon = one_item.getElementsByTagName('longitude').item(0).textContent;
					lat = one_item.getElementsByTagName('latitude').item(0).textContent;

					phone = one_item.getElementsByTagName('phone').item(0).textContent;

					open_items = one_item.getElementsByTagName('open');

					var openStr = [];
					for (var j = 0; j < open_items.length; j++){
						var one_open_item = open_items.item(j);
						openStr.push(one_open_item.textContent);

						logg(one_open_item.getAttribute('desc'));
					} // end for-loop j
					// logg('openStr.length = '+openStr.length);

					var dict = {
						adress: adress,
						latitude: lat,
						longitude: lon,
						phone: phone,
						open1: openStr[0],
						open2: openStr[1],
						open3: openStr[2]
					};
					// logg('dict = ', dict, true);
					xmlData.push(dict);
				} //end for-loop i

				if (callback) {
					callback('done', xmlData);
				}
				logg('length = '+xmlData.length);
			} catch (err) {
				//det gick inte att "parsa" json-filen
				alert('XML error!\n' + err.message);
				if (callback) {
					callback('error', '');
				}
			}
		} else {
			alert('Nåt annat gick fel... #' + e.code + ' message:' + e.error);
			if (callback) {
				callback('error', '');
			}
		}
	};

	//funktion som utförs ifall nåt gick fel med uppkoppling mot nätet eller om servern vi anropar inte svarar m.m
	// eller om det blir en "timeout", dvs servern hinner inte svara av olika anledningar
	xhr.onerror = function(err) {
		// variabeln err innehåller felkod ("code") och felmeddelande ("error")
		alert('Nåt gick fel med uppkopplingen!\n' + err.code + ' ' + err.error);
	};

	//
	xhr.open("GET", url);

	xhr.send();
}
exports.loadXML = loadXML;


/**
 * downloads a file and saves it to application data directory
 *
 * - parameter downloadDict: a dictionary with properties httpLurlink och fileName
 * 		url: "" (required) link to file on server, eg: "http://www.domain.com/mysite/file.png"
 * 		fileName: "" (optional) name to save file as..., eg: "image.png"
 * - parameter callback: the current state (error, porgress and success) will be passed to the callback, on success the filenamne is also returned
 */
function downloadFile(url, callback) {

	xhr = Titanium.Network.createHTTPClient();
	xhr.setTimeout(60000);

	xhr.onload = function() {
		logg('downloadFile(), onload...');

		if (callback) {
			callback('done', this.responseData);
		}
	};

	xhr.ondatastream = function(e) {
		logg('ondatastream(), e =', e);

		if (callback) {
			callback('updateProgressBar', e.progress);
		}
	};

	xhr.onerror = function(err) {
		logg('downloadFile(), onerror, err = ', err);

		err.msg = getHttpErrorText(err.code);

		if (callback) {
			callback('downloadError', err);
		}
	};

	logg('downloadFile(), url = ' + url);

	xhr.open('GET', url);

	xhr.send();
}

exports.downloadFile = downloadFile;

/**
 * example of getting data from an open API
 */
function getWeather(callback) {
	var json;

	var API_key = 'b1b15e88fa797225412429c1c50c122a1';
	//dmeo API-key

	// weather from gps pos
	// 'http://api.openweathermap.org/data/2.5/weather?lat=55.538685&lon=13.952695&units=metric&appid='+API_key

	// get weather icon
	// 'http://openweathermap.org/img/w/01d.png'

	var units = 'metric';
	// 'imperial'
	var q = 'London,uk';

	// get weather for cityname and country
	//var url = 'http://api.openweathermap.org/data/2.5/weather?q='+q+'&units='+units+'&appid='+API_key;

	//demo API
	var url = 'http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1';

	var xhr = Ti.Network.createHTTPClient();
	xhr.setTimeout(25000);

	xhr.onload = function() {
		logg('getWeather(), onload >> responseText = ' + this.responseText);
		try {
			json = JSON.parse(this.responseText);

			if (callback) {
				callback('done', json);
			}
		} catch(err) {
			logg('getWeather(), CATCH-error ', err);

			if (callback) {
				callback('error', err);
			}
		}
	};

	xhr.onerror = function(err) {
		logg('getWeather(), onerror >>', err, false, {
			error: true
		});

		// alert(L('onerror') + ' code: ' + getHttpErrorText(err.code));

		err.msg = getHttpErrorText(err.code);

		if (callback) {
			callback('error', err);
		}
	};

	logg('getWeather(), url  >>  ' + url);

	xhr.open('GET', url);

	xhr.send();

	/**
	 exempel på svar:

	 {
	 "coord": {
	 "lon": 13.95,
	 "lat": 55.54
	 },
	 "weather": [
	 {
	 "id": 800,
	 "main": "Clear",
	 "description": "clear sky",
	 "icon": "01d" // fetch icon >> http://openweathermap.org/img/w/01d.png
	 }
	 ],
	 "base": "cmc stations",
	 "main": {
	 "temp": 12.48, // Celsius eller Fahrenheit beroende på &units
	 "pressure": 1025, // hPa
	 "humidity": 50,  // percent
	 "temp_min": 10.15, // Celsius eller Fahrenheit beroende på &units
	 "temp_max": 14.35  // Celsius eller Fahrenheit beroende på &units
	 },
	 "wind": {
	 "speed": 7.7,  // meters / sec
	 "gust": 13.4,  // vindbyar (m/s)
	 "deg": 300	// degrees (meteorological)
	 },
	 "clouds": {
	 "all": 0 // percent
	 },
	 "dt": 1461165600, //timestamp UTC
	 "sys": {
	 "type": 3,
	 "id": 5397,
	 "message": 0.0035,
	 "country": "SE",
	 "sunrise": 1461123915,
	 "sunset": 1461176511
	 },
	 "id": 2668365,
	 "name": "Tomelilla",
	 "cod": 200
	 }
	 */
}

exports.getWeather = getWeather;

function getHttpErrorText(errorCode) {
	var n = errorCode;
	switch (n) {
		case 100:
			return 'Continue';
		//  This means that the server has received the request headers, and that the client should proceed to send the request body (in the case of a request for which a body needs to be sent; for example, a POST request). If the request body is large, sending it to a server when a request has already been rejected based upon inappropriate headers is inefficient. To have a server check if the request could be accepted based on the request's headers alone, a client must send Expect: 100-continue as a header in its initial request[2] and check if a 100 Continue status code is received in response before continuing (or receive 417 Expectation Failed and not continue).[2]
		case 101:
			return 'Switching Protocols';
		//  This means the requester has asked the server to switch protocols and the server is acknowledging that it will do so.[2]
		case 102:
			return 'Processing';
		//  (WebDAV) (RFC 2518) As a WebDAV request may contain many sub-requests involving file operations, it may take a long time to complete the request. This code indicates that the server has received and is processing the request, but no response is available yet.[3] This prevents the client from timing out and assuming the request was lost.
		case 122:
			return 'Request-URI too long';
		//  This is a non-standard IE7-only code which means the URI is longer than a maximum of 2083 characters.[4][5] (See code 414.)
		case 200:
			return 'OK';
		//  Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request the response will contain an entity describing or containing the result of the action.[2]
		case 201:
			return 'Created';
		//  The request has been fulfilled and resulted in a new resource being created.[2]
		case 202:
			return 'Accepted';
		//  The request has been accepted for processing, but the processing has not been completed. The request might or might not eventually be acted upon, as it might be disallowed when processing actually takes place.[2]
		case 203:
			return 'Non-Authoritative Information';
		//  (since HTTP/1.1) The server successfully processed the request, but is returning information that may be from another source.[2]
		case 204:
			return 'No Content';
		//  The server successfully processed the request, but is not returning any content.[2]
		case 205:
			return 'Reset Content';
		//  The server successfully processed the request, but is not returning any content. Unlike a 204 response, this response requires that the requester reset the document view.[2]
		case 206:
			return 'Partial Content';
		//  The server is delivering only part of the resource due to a range header sent by the client. The range header is used by tools like wget to enable resuming of interrupted downloads, or split a download into multiple simultaneous streams.[2]
		case 207:
			return 'Multi-Status';
		//  (WebDAV) (RFC 4918) The message body that follows is an XML message and can contain a number of separate response codes, depending on how many sub-requests were made.[6]
		case 226:
			return 'IM Used';
		//  (RFC 3229) The server has fulfilled a GET request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance. [7]
		case 300:
			return 'Multiple Choices';
		// Indicates multiple options for the resource that the client may follow. It, for instance, could be used to present different format options for video, list files with different extensions, or word sense disambiguation.[2]
		case 301:
			return 'Moved Permanently';
		// This and all future requests should be directed to the given URI.[2]
		case 302:
			return 'Found';
		// This is an example of industrial practice contradicting the standard.[2] HTTP/1.0 specification (RFC 1945) required the client to perform a temporary redirect (the original describing phrase was "Moved Temporarily"),[8] but popular browsers implemented 302 with the functionality of a 303 See Other. Therefore, HTTP/1.1 added status codes 303 and 307 to distinguish between the two behaviours.[9] However, the majority of Web applications and frameworks still[as of?] use the 302 status code as if it were the 303.[citation needed]
		case 303:
			return 'See Other';
		// (since HTTP/1.1) The response to the request can be found under another URI using a GET method. When received in response to a POST (or PUT/DELETE), it should be assumed that the server has received the data and the redirect should be issued with a separate GET message.[2]
		case 304:
			return 'Not Modified';
		// Indicates the resource has not been modified since last requested.[2] Typically, the HTTP client provides a header like the If-Modified-Since header to provide a time against which to compare. Using this saves bandwidth and reprocessing on both the server and client, as only the header data must be sent and received in comparison to the entirety of the page being re-processed by the server, then sent again using more bandwidth of the server and client.
		case 305:
			return 'Use Proxy';
		// (since HTTP/1.1) Many HTTP clients (such as Mozilla[10] and Internet Explorer) do not correctly handle responses with this status code, primarily for security reasons.[2]
		case 306:
			return 'Switch Proxy';
		// No longer used.[2]
		case 307:
			return 'Temporary Redirect';
		// (since HTTP/1.1) In this occasion, the request should be repeated with another URI, but future requests can still use the original URI.[2] In contrast to 303, the request method should not be changed when reissuing the original request. For instance, a POST request must be repeated using another POST request.
		case 400:
			return 'Bad Request';
		// The request cannot be fulfilled due to bad syntax.[2]
		case 401:
			return 'Unauthorized';
		// Similar to 403 Forbidden, but specifically for use when authentication is possible but has failed or not yet been provided.[2] The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication.
		case 402:
			return 'Payment Required';
		// Reserved for future use.[2] The original intention was that this code might be used as part of some form of digital cash or micropayment scheme, but that has not happened, and this code is not usually used. As an example of its use, however, Apple's MobileMe service generates a 402 error ("httpStatusCode:402" in the Mac OS X Console log) if the MobileMe account is delinquent.
		case 403:
			return 'Forbidden';
		// The request was a legal request, but the server is refusing to respond to it.[2] Unlike a 401 Unauthorized response, authenticating will make no difference.[2]
		case 404:
			return 'Not Found';
		// The requested resource could not be found but may be available again in the future.[2] Subsequent requests by the client are permissible.
		case 405:
			return 'Method Not Allowed';
		// A request was made of a resource using a request method not supported by that resource;[2] for example, using GET on a form which requires data to be presented via POST, or using PUT on a read-only resource.
		case 406:
			return 'Not Acceptable';
		// The requested resource is only capable of generating content not acceptable according to the Accept headers sent in the request.[2]
		case 407:
			return 'Proxy Authentication Required';
		//[2]
		case 408:
			return 'Request Timeout';
		// The server timed out waiting for the request.[2] According to W3 HTTP specifications: "The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time."
		case 409:
			return 'Conflict';
		// Indicates that the request could not be processed because of conflict in the request, such as an edit conflict.[2]
		case 410:
			return 'Gone';
		// Indicates that the resource requested is no longer available and will not be available again.[2] This should be used when a resource has been intentionally removed and the resource should be purged. Upon receiving a 410 status code, the client should not request the resource again in the future. Clients such as search engines should remove the resource from their indices. Most use cases do not require clients and search engines to purge the resource, and a "404 Not Found" may be used instead.
		case 411:
			return 'Length Required';
		// The request did not specify the length of its content, which is required by the requested resource.[2]
		case 412:
			return 'Precondition Failed';
		// The server does not meet one of the preconditions that the requester put on the request.[2]
		case 413:
			return 'Request Entity Too Large';
		// The request is larger than the server is willing or able to process.[2]
		case 414:
			return 'Request-URI Too Long';
		// The URI provided was too long for the server to process.[2]
		case 415:
			return 'Unsupported Media Type';
		// The request entity has a media type which the server or resource does not support.[2] For example, the client uploads an image as image/svg+xml, but the server requires that images use a different format.
		case 416:
			return 'Requested Range Not Satisfiable';
		// The client has asked for a portion of the file, but the server cannot supply that portion.[2] For example, if the client asked for a part of the file that lies beyond the end of the file.
		case 417:
			return 'Expectation Failed';
		// The server cannot meet the requirements of the Expect request-header field.[2]
		case 418:
			return 'I\'m a teapot';
		// This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers.
		case 422:
			return 'Unprocessable Entity';
		// (WebDAV) (RFC 4918) The request was well-formed but was unable to be followed due to semantic errors.[6]
		case 423:
			return 'Locked';
		// (WebDAV) (RFC 4918) The resource that is being accessed is locked.[6]
		case 424:
			return 'Failed Dependency';
		// (WebDAV) (RFC 4918) The request failed due to failure of a previous request (e.g. a PROPPATCH).[6]
		case 425:
			return 'Unordered Collection';
		// (RFC 3648) Defined in drafts of "WebDAV Advanced Collections Protocol",[11] but not present in "Web Distributed Authoring and Versioning (WebDAV) Ordered Collections Protocol".[12]
		case 426:
			return 'Upgrade Required';
		// (RFC 2817) The client should switch to a different protocol such as TLS/1.0.[13]
		case 444:
			return 'No Response';
		// An Nginx HTTP server extension. The server returns no information to the client and closes the connection (useful as a deterrent for malware).
		case 449:
			return 'Retry With';
		// A Microsoft extension. The request should be retried after performing the appropriate action.[14]
		case 450:
			return 'Blocked by Windows Parental Controls';
		// A Microsoft extension. This error is given when Windows Parental Controls are turned on and are blocking access to the given webpage.[15]
		case 499:
			return 'Client Closed Request';
		// An Nginx HTTP server extension. This code is introduced to log the case when the connection is closed by client while HTTP server is processing its request, making server unable to send the HTTP header back.[16]
		case 500:
			return 'Internal Server Error';
		// A generic error message, given when no more specific message is suitable.[2]
		case 501:
			return 'Not Implemented';
		// The server either does not recognise the request method, or it lacks the ability to fulfill the request.[2]
		case 502:
			return 'Bad Gateway';
		// The server was acting as a gateway or proxy and received an invalid response from the upstream server.[2]
		case 503:
			return 'Service Unavailable';
		// The server is currently unavailable (because it is overloaded or down for maintenance).[2] Generally, this is a temporary state.
		case 504:
			return 'Gateway Timeout';
		// The server was acting as a gateway or proxy and did not receive a timely response from the upstream server.[2]
		case 505:
			return 'HTTP Version Not Supported';
		// The server does not support the HTTP protocol version used in the request.[2]
		case 506:
			return 'Variant Also Negotiates';
		// (RFC 2295) Transparent content negotiation for the request results in a circular reference.[17]
		case 507:
			return 'Insufficient Storage';
		// (WebDAV) (RFC 4918)[6]
		case 509:
			return 'Bandwidth Limit Exceeded';
		// (Apache bw/limited extension) This status code, while used by many servers, is not specified in any RFCs.
		case 510:
			return 'Not Extended';
		// (RFC 2774) Further extensions to the request are required for the server to fulfill it.[18]
		default:
			return 'Unknown HTTP status code: ' + n;
	}
}

exports.getHttpErrorText = getHttpErrorText;

//prevent links from being cached
function addRandomTo(url) {
	logg('addRandomTo(' + url + ')...');
	var rand = "";
	if (url.search("/\?\=") == -1) {
		rand = "?ran=" + Math.random();
	} else {
		rand = "&ran=" + Math.random();
	}

	logg(' --> new URL = ' + url + rand);
	return url + rand;
}

exports.addRandomTo = addRandomTo;

