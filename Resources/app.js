
/* XXX
 * 	Vi skapar globala variablar av fc och appFuncs
 * 		så vi kommer åt de överallt i appen, i alla js-filer
 */
var fc = require('fonts_and_colors');
var appFuncs = require('app_functions');


//XXX en ännu kortare väg för att komma åt vår bakgrundsfärg (fortfarande globalt åtkomlig)
var bgColor = fc.colors.winBg;


/* XXX
 * 	Vi förkortar/förenklar vissa variablar som vi kommer behöva oftare,
 * 		på detta sätt slipper vi skriva: appFuncs.OS_is_Android
 * 		utan kan förkorta det till: OS_is_Android
 */
var OS_is_Android = appFuncs.OS_is_Android,
	OS_is_iOS = appFuncs.OS_is_iOS,
	logg = appFuncs.logg,
	devColor = appFuncs.devColor;

//XXX en självkörande funktion, som skapar en tabGroup och öppnar den (så appen startar)
(function() {
	/* XXX
	 * 	Här laddar vi filen (modulen): "ApplicationTabGroup" och sparar en
	 * 		referens till den i variabeln ApplicationTabGroup
	 *
	 */
	var ApplicationTabGroup = require('ApplicationTabGroup');
	/* XXX
	 * 	Vi initierar ApplicationTabGroup med ()
	 *  		och öppnar sen tabGroupen direkt med open() metoden.
	 *
	 * 	Om vi vill kunna ändra eller sätta tex vilken tab som ska vara aktiv,
	 * 		behöver vi spara en referens till den i en variabel
	 * 		(det gör vi inte i nuläget)
	 */
	new ApplicationTabGroup().open();
})();

