/**
 **
 **   Bara funktioner och variablar i denna fil !!
 **
 **	 >> Denna fil finns i alla fönster / filer
 **
 **/

/*
 * OS type and version
 */
var OS_is_Android = Ti.Platform.osname == "android";
exports.OS_is_Android = OS_is_Android;

var OS_is_iOS = !OS_is_Android;
exports.OS_is_iOS = OS_is_iOS;

var OS_is_iOS9OrLower = false,
	OS_is_iOS9 = false,
	OS_is_iOS10 = false,
	OS_is_iOS11 = false;
if (OS_is_iOS){
	var ver = parseInt(Ti.Platform.version, 10);
	OS_is_iOS9OrLower = ver < 10;
	OS_is_iOS9 = ver >= 9 && ver < 10;
	OS_is_iOS10 = ver >= 10 && ver < 11;
	OS_is_iOS11 = ver >= 11 && ver < 12;
}
exports.OS_is_iOS9OrLower = OS_is_iOS9OrLower;
exports.OS_is_iOS9 = OS_is_iOS9;
exports.OS_is_iOS10 = OS_is_iOS10;
exports.OS_is_iOS11 = OS_is_iOS11;


/*
 * Device types
 */
var device_is_iPad = Ti.Platform.osname === 'ipad';
exports.device_is_iPad = device_is_iPad;

var device_is_iPhone = Ti.Platform.osname === 'iphone';
exports.device_is_iPhone = device_is_iPhone;

var device_is_PhoneType = !device_is_iPad;
exports.device_is_PhoneType = device_is_PhoneType;

var device_is_Simulator = ((Ti.Platform.model.toUpperCase() == "SIMULATOR") || (Ti.Platform.model.toUpperCase() == "GOOGLE_SDK"));
exports.device_is_Simulator = device_is_Simulator;


/*
 * Orientation & screen dimensions
 */
var isLandscape = function() {
	return Ti.Gesture.landscape;
};
exports.isLandscape = isLandscape;

var screenW = function(useAndroidDpiFactor) {
	var sW = Ti.Platform.displayCaps.platformWidth;
	if (OS_is_Android){
		var sH = Ti.Platform.displayCaps.platformHeight;
		if (sW > sH) {
			//device is rotated but win is locked to either portrait or landscape
			sW = sH;
		}
	}
	if (useAndroidDpiFactor){
		return sW / dpiFactor;
	} else {
		return sW;
	}
};
exports.screenW = screenW;

var screenH = function(useAndroidDpiFactor) {
	var sH = Ti.Platform.displayCaps.platformHeight;
	if (OS_is_Android){
		var sW = Ti.Platform.displayCaps.platformWidth;
		if (sW > sH) {
			//device is rotated but win is locked to either portrait or landscape
			sH = sW;
		}
	}
	if (useAndroidDpiFactor){
		return sH / dpiFactor;
	} else {
		return sH;
	}
};
exports.screenH = screenH;


/*
 * Device screen dimensions
 */
var isRetina = Ti.Platform.displayCaps.density == 'high';
var is_35Inch = screenH() == 480 || screenW() == 480;
var is_4Inch  = screenH() == 568 || screenW() == 568;
var is_47Inch = screenH() == 667 || screenW() == 667;
var is_55Inch = screenH() == 736 || screenW() == 736;
var is_58Inch = screenH() == 812 || screenW() == 812;

var is_at3x =  Ti.Platform.displayCaps.dpi >= 480; //iPhone Plus modeller
var dpiScaleFactor = device_is_iPad ? 1.35 : is_at3x ? 1.25 : 1;

exports.isRetina = isRetina;
exports.is_35Inch = is_35Inch;
exports.is_4Inch = is_4Inch;
exports.is_47Inch = is_47Inch;
exports.is_55Inch = is_55Inch;
exports.is_58Inch = is_58Inch;
exports.is_at3x = is_at3x;
exports.dpiScaleFactor = dpiScaleFactor;

// iPhone X
var device_is_iPhoneX = is_58Inch && OS_is_iOS;
exports.device_is_iPhoneX = device_is_iPhoneX;

// iPhone X safe areas
var safeAreaTop_iPhoneX = 44;
exports.safeAreaTop_iPhoneX = safeAreaTop_iPhoneX;

var safeAreaBottom_iPhoneX = 34;
exports.safeAreaBottom_iPhoneX = safeAreaBottom_iPhoneX;


// Android density (screen resolution)
var density = Ti.Platform.displayCaps.density;
var dpiFactor = 1; //mdpi
if (density == 'high') {
	dpiFactor = 1.5;
} else if (density == 'xhigh') {
	dpiFactor = 2;
} else if (density == 'xxhigh') {
	dpiFactor = 3;
} else if (density == 'xxxhigh') {
	dpiFactor = 3;
}

if (OS_is_iOS){
	var narrowScreen = screenW() <= 320;
	var shortScreen = screenH() <= 480;
} else{
	//Android
	var narrowScreen = (screenW()/dpiFactor) < 380;
	var shortScreen = (screenH()/dpiFactor) < 520;
}
exports.narrowScreen = narrowScreen;
exports.shortScreen = shortScreen;

var smallScreen = (narrowScreen || shortScreen);
exports.smallScreen = smallScreen;


var iPhoneModel = 99; //Android
if (OS_is_iOS){
	if (is_35Inch && isRetina) {
		// 4S
		iPhoneModel = 4;
	} else if (is_4Inch) {
		// 5
		iPhoneModel = 5;
	}else if (is_47Inch) {
		// 6, 7, 8
		iPhoneModel = 6;
	}else if (is_55Inch){
		// 6, 7, 8 plus
		iPhoneModel = 6.5;
	}else if (is_58Inch){
		// X
		iPhoneModel = 10;
	}else {
		if (device_is_iPad){
			iPhoneModel = 999;//iPad
		}else{
			// 4 or 3GS
			iPhoneModel = 3;
		}
	}
}
exports.iPhoneModel = iPhoneModel;


if (OS_is_Android){
	var androidSoftInput = Ti.UI.Android.SOFT_INPUT_STATE_ALWAYS_HIDDEN | Ti.UI.Android.SOFT_INPUT_ADJUST_PAN;
	exports.androidSoftInput = androidSoftInput;
}
var ScreenIsLowDpi = Ti.Platform.displayCaps.density == 'low';
exports.ScreenIsLowDpi = ScreenIsLowDpi;


Ti.API.info('***********************************************************');
Ti.API.info('Ti SDK-version                  = '+Ti.version);
Ti.API.info('Ti.Platform.osname              = '+Ti.Platform.osname);
Ti.API.info('Ti.Platform.version             = '+Ti.Platform.version);
Ti.API.info('Ti.Platform.model               = '+Ti.Platform.model.toUpperCase());
Ti.API.info('Ti.Platform.displayCaps.dpi     = '+Ti.Platform.displayCaps.dpi);
Ti.API.info('Ti.Platform.displayCaps.density = '+Ti.Platform.displayCaps.density);
Ti.API.info('Ti.Platform.id                  = '+Ti.Platform.id);
Ti.API.info('Ti.App.id                       = '+Ti.App.id);
Ti.API.info('Ti.App.guid                     = '+Ti.App.guid);
Ti.API.info('Ti.App.sessionId                = '+Ti.App.sessionId);
Ti.API.info('Ti.App.installId                = '+Ti.App.installId);
Ti.API.info(' ');
Ti.API.info('  >> device_is_Simulator  = '+device_is_Simulator);
Ti.API.info('  >> device_is_iPad       = '+device_is_iPad);
Ti.API.info('  >> device_is_iPhone     = '+device_is_iPhone);
Ti.API.info('  >> device_is_PhoneType  = '+device_is_PhoneType);
Ti.API.info('  >> device_is_iPhoneX    = '+device_is_iPhoneX);
Ti.API.info('  >> isRetina             = '+isRetina);
Ti.API.info('  >> is_35Inch            = '+is_35Inch);
Ti.API.info('  >> is_4Inch             = '+is_4Inch);
Ti.API.info('  >> is_47Inch            = '+is_47Inch);
Ti.API.info('  >> is_55Inch            = '+is_55Inch);
Ti.API.info('  >> is_58Inch            = '+is_58Inch);
Ti.API.info('  >> is_at3x              = '+is_at3x);
Ti.API.info('  >> isLandscape          = '+isLandscape());
Ti.API.info('  >> iPhoneModel          = '+iPhoneModel);
Ti.API.info('  >> screen width         = '+screenW());
Ti.API.info('  >> screen height        = '+screenH());
Ti.API.info('  >> narrowScreen         = '+narrowScreen);
Ti.API.info('  >> shortScreen          = '+shortScreen);
Ti.API.info('  >> smallScreen          = '+smallScreen);
Ti.API.info('  >> ScreenIsLowDpi       = '+ScreenIsLowDpi);
Ti.API.info(' ');
Ti.API.info('***********************************************************');

var debugMode = device_is_Simulator ? true : false;//!! TODO: SKA vara true : false i släppt version!

var defBorderWidth = 2;
if (isRetina == false) {
	defBorderWidth = 1;
}


Ti.App.addEventListener('memorywarning', function(e) {
	Ti.API.error(' #####################################');
	Ti.API.error(' #####################################');
	Ti.API.error(' ####                             ####');
	Ti.API.error(' ####                             ####');
	Ti.API.error(' ####                             ####');
	Ti.API.error(' #### app recieved MEMORY WARNING ####');
	Ti.API.error(' ####                             ####');
	Ti.API.error(' ####                             ####');
	Ti.API.error(' ####                             ####');
	Ti.API.error(' #####################################');
	Ti.API.error(' #####################################');
});



function generateString(str, noOfRepeats) {
   	noOfRepeats = noOfRepeats || 1;
    return Array(noOfRepeats+1).join(str);
}
exports.generateString = generateString;


function isInRange(val, min, max) {
	return val >= min && val <= max;
}
exports.isInRange = isInRange;


function getPosNegRandom(value) {
	posOrNeg = getRandomInRange(-1, 1);

	ret = value * (posOrNeg / Math.abs(posOrNeg));

	if (debugCalc){
		// logg('getPosNegRandom(), value = '+value+', posOrNeg = '+posOrNeg+', return = '+ret);
	}

	return ret;
}
exports.getPosNegRandom = getPosNegRandom;

function getPosNegRandomInRange(min, max) {
	var ran = Math.random() * (max - min) + min;

	posOrNeg = getRandomInRange(-1, 1);

	ret = ran * (posOrNeg / Math.abs(posOrNeg));

	if (debugCalc){
		// logg('getPosNegRandomInRange(), ran = '+ran+', posOrNeg = '+posOrNeg+', return = '+ret);
	}

	return ret;
}
exports.getPosNegRandomInRange = getPosNegRandomInRange;

function getRandomInRange(min, max) {
	return Math.random() * (max - min) + min;
}
exports.getRandomInRange = getRandomInRange;


var touchDelay = 300;
exports.touchDelay = touchDelay;

function addTouchyFeeling(theObject, offsetOpacity) {
	theObject.offsetOpacity = offsetOpacity;
	theObject.addEventListener('touchstart', function(e) {
		// logg('addTouchyFeeling, event >> "touchstart"...');

		if (offsetOpacity === undefined){
			theObject.opacity = 0.5;
		}else{
			theObject.opacity = theObject.opacity * theObject.offsetOpacity;
		}
	});
	theObject.addEventListener('touchend', function(e) {
		// logg('addTouchyFeeling, event >> "touchend"...');
		theObject.touchEnabled = false;
		setTimeout(function(e) {
			if (theObject.isOn !== undefined){
				if (theObject.isOn){
					theObject.opacity = 1.0;
				}else{
					theObject.opacity = 0.5;
				}
			}else{
				theObject.opacity = 1.0;
			}
		}, touchDelay);
		var _object = theObject;
		setTimeout(function(e) {
			// logg('addTouchyFeeling, _object = '+_object);
			_object.touchEnabled = true;
		}, 350);
	});
	theObject.addEventListener('touchcancel', function(e) {
		// logg('addTouchyFeeling, event >> "touchcancel"...');
		setTimeout(function(e) {
			theObject.opacity = 1.0;
		}, touchDelay);
	});
}
exports.addTouchyFeeling = addTouchyFeeling;


function logg(msg, object, enumerateObject, loggData) {

	//kollar om vi är i "debug"-mode
	if (!debugMode) {
		return;
	}

	if (enumerateObject === undefined){
		enumerateObject = false;
	}
	if (typeof msg == 'object' && object === undefined) {
		Ti.API.debug('logg() has detected "msg" is [object], try to stringify "msg"...');
		object = msg;
		msg = '';
	}

	if ((enumerateObject == true) && (typeof object == 'object')){
		Ti.API.info('logg(), will try to enumerate [object]...');
		Ti.API.info('#########################################################');
		try {
			for (x in object) {
				if (typeof object[x] == 'object'){
				    objStr = ' object['+x+'] = ' + JSON.stringify(object[x]);
				}else{
					objStr = ' object['+x+'] = ' + object[x];
			   }
				Ti.API.info(msg+objStr);
			}
		} catch(err) {
			Ti.API.error('logg(), UNABLE to enumerate [object]!! err = '+err);
		}
		Ti.API.info('#########################################################');
		Ti.API.info('');
	}else{
		enumerateObject = false; //object var inte ett [object]
		var objStr = '';
		if (object !== undefined){
			try {
				objStr = ' '+JSON.stringify(object);
			} catch(err) {
				Ti.API.warn('## catch-ERROR in function logg(): UNABLE TO STRINGIFY "object".');
			}
		}
		if (loggData !== undefined){
			if (loggData.error) {
				Ti.API.error(msg + objStr);

			}else if (loggData.warn) {
				Ti.API.warn(msg + objStr);

			}else{
				Ti.API.info(msg + objStr);

			}
		}else{
			Ti.API.info(msg+objStr);

		}
	}
};
exports.logg = logg;


function devColor(theObject, theColor, theOpacity) {
	if (!debugMode) {
		logg('devColor() debugMode = false');
		return;
	}
	if (theOpacity === undefined) {
		// logg('devColor() theOpacity is undefined, setting it to 0.5...');
		theOpacity = 0.5;
	}
	if (theColor === undefined) {
		// logg('devColor() theColor is undefined, setting it to random...');
		theColor = 'random';
	}
	if (theColor == 'random') {
		// logg('devColor() theColor is random');
		theColor = Math.floor(Math.random() * 16777215).toString(16);
		if (theColor.length < 6) {
			theColor = '0' + theColor;
		}
		theColor = '#' + theColor;
	}
	if (theObject !== null){
		if (theObject !== undefined){
			// logg('theColor = ' + theColor);
			theObject.backgroundColor = theColor;
			theObject.opacity = theOpacity;
		}
	}
}
exports.devColor = devColor;


function alertInput(alertDict, callback) {
	var dialog = Ti.UI.createAlertDialog({
		title: alertDict.title ? alertDict.title : 'Input',
		message: alertDict.message ? alertDict.message : 'Your input: ',
		buttonNames: alertDict.btns ? alertDict.btns : ['Cancel', 'OK'],
		style: Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,
		keyboardType: alertDict.keyBoard ? alertDict.keyBoard : Ti.UI.KEYBOARD_TYPE_DEFAULT
	});

	dialog.addEventListener('click', function(e) {
		if (callback) {
			callback(e.index, e.text);
		}
	});

	dialog.show();
}
exports.alertInput = alertInput;


function alertD(msg, the_title){
	if (the_title == undefined){
		the_title = 'Information';
	}
	var dialog = Ti.UI.createAlertDialog({
		message: msg,
		ok: 'OK',
		title: the_title
	}).show();
}
exports.alertD = alertD;


function alertD_reply(title, msg, btns, optionsDict) {
	// logg('alertD_reply(), optionsDict = ',optionsDict);

	if (optionsDict == undefined){
		optionsDict = {};
	}
	if (title == '') {
		title = 'Information';
	}
	if (btns.length == 0) {
		btns.push('OK');
	}

	var dialog = Ti.UI.createAlertDialog({
		message: msg,
		buttonNames: btns,
		title: title,
		cancel: optionsDict.cancelID ? optionsDict.cancelID : btns.length-1
	});
	return dialog;
}
exports.alertD_reply = alertD_reply;


function pc(percent, inRelationToObject) {
	var screen_W = Ti.Platform.displayCaps.platformWidth;
	var screen_H = Ti.Platform.displayCaps.platformHeight;
	if (inRelationToObject !== undefined){
		//Nu kan man räkna i procent till andra objekts bredd eller höjd
		screen_W = inRelationToObject.width;
		screen_H = inRelationToObject.height;
	}

	var percentSign = percent.search('%');
	if (percentSign > -1){
		percent = percent.replace('%', '');
	}
	percent = parseFloat(percent) / 100;

	//logg('pc(), percent = '+percent+', screen_W*percent = '+screen_W*percent+', screen_H*percent = '+screen_H*percent);

	return { hori: screen_W * percent, vert: screen_H * percent };
}
exports.pc = pc;


function addVerticalDividerLine(data) {
	if (data.color === undefined) {
		data.color = fc.colors.dividerLine;
	}
	return Ti.UI.createView({
		width: data.width !== undefined ? data.width : 0.5,
		height: data.height,
		top: data.top,
		left: data.left,
		right: data.right,
		bottom: data.bottom,
		backgroundColor: data.color,
		touchEnabled: false
	});
}
exports.addVerticalDividerLine = addVerticalDividerLine;

function addDividerLineCustom(divDict) {
	if (divDict === undefined) {
		divDict = {};
	}

	return Ti.UI.createView({
		width: divDict.width ? divDict.width : '95%',
		height: divDict.height ? divDict.height : defBorderWidth,
		top: divDict.top,
		bottom: divDict.bottom,
		backgroundColor: divDict.color ? divDict.color : fc.colors.dividerLine,
		touchEnabled: false
	});
}
exports.addDividerLineCustom = addDividerLineCustom;


function addDividerLine(pTop, pBottom, pColor) {
	if (pColor === undefined) {
		pColor = fc.colors.dividerLine;
	}
	return Ti.UI.createView({
		width: '100%',
		height: defBorderWidth,
		top: pTop,
		bottom: pBottom,
		backgroundColor: pColor,
		touchEnabled: false
	});
}
exports.addDividerLine = addDividerLine;


function addSomeCustomSpace(spaceDict) {
	return Ti.UI.createView({
		top: spaceDict.top,
		left: spaceDict.left,
		right: spaceDict.right,
		bottom: spaceDict.bottom,
		height: spaceDict.height,
		width: spaceDict.width,
		touchEnabled: false
	});
}
exports.addSomeCustomSpace = addSomeCustomSpace;


function addSomeSpace(vHeight) {
	return Ti.UI.createView({
		height: vHeight,
		width: '100%',
		touchEnabled: false
	});
}
exports.addSomeSpace = addSomeSpace;


function getNow(){
	return new Date().getTime();
}
exports.getNow = getNow;


function hideTheKeyboard(fieldHasFocus){
	if (fieldHasFocus !== undefined && fieldHasFocus !== null){
		fieldHasFocus.blur();
	}
}
exports.hideTheKeyboard = hideTheKeyboard;


function fTime(timeDict) {
	var timeStr = '';
	var hh,mm,ss,ms;

	if (timeDict.time_in_mins !== undefined) {
		mm = Math.floor(timeDict.time_in_mins);
		ss = Math.round((timeDict.time_in_mins - mm) * 60);
		if (ss < 10){
			ss = '0'+ss;
		}
		timeStr = mm+':'+ss;

	} else if (timeDict.time_in_ms !== undefined) {
		ss = timeDict.time_in_ms / 1000;

	    // 2- Extract hours:
	    var hh = parseInt(ss / 3600); // 3,600 seconds in 1 hour
	    ss = ss % 3600; // seconds remaining after extracting hours
	    // 3- Extract minutes:
	    var mm = parseInt(ss / 60); // 60 seconds in 1 minute
	    // 4- Keep only seconds not extracted to minutes:
	    ss = Math.round(ss % 60);

		if (mm >= 60){
			m = 0;
		}
		if (ss >= 60){
			ss = 0;
		}
		// logg(' hh = '+hh+', mm = '+mm+', ss = '+ss);

		if (mm < 10 && hh > 0){
			mm = '0'+mm;
		}
		if (ss < 10) {
			ss = '0'+ss;
		}
		if (hh > 0){
			timeStr = hh+':'+mm+':'+ss;
		}else{
			timeStr = mm+':'+ss;
		}
	} else{

	}

	return timeStr;
}
exports.fTime = fTime;

