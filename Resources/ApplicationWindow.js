
//XXX modlen tar emot två värden när den skapas, en title och gpsData (som ska visas i detta fönster)
function ApplicationWindow(title, gpsData) {
	var win = Ti.UI.createWindow({
		title: title,
		backgroundColor: 'white'
	});

	logg('ApplicationWindow(), gpsData = ', gpsData);

	//XXX vi visar telefonnr på knappen.
	var phoneBtn = Ti.UI.createButton({
		title: gpsData.phone,
		textAlign: 'center',
		color: fc.colors.blackFont,
		font: fc.fonts[14]['normal'],
		top: 20
	});
	win.add(phoneBtn);

	phoneBtn.addEventListener('click', function(e) {
		//XXX om man vill öppna en http-länk, skicka mail eller ringa ett tel-nr eller öppna iMessage-appen med ett förvalt nummer/epost
		//används nedan stående rad
		// länk anges med "http://www..."
		//		tel med "tel:12345678"
		//		sms med "sms:12345678"
		// epost med "mailto:info@domain.com"
		Ti.Platform.openURL('tel:'+phoneBtn.title);
	});

	return win;
};

module.exports = ApplicationWindow;
