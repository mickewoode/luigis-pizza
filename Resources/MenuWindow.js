function ApplicationWindow(title) {

	var httpFuncs = require('http_funcs.js');

	//XXX en array för att spara våra rader i
	var twRows = [];

	//XXX en räknare för att hålal koll på hur många rader som är förkryssade
	var checkedRows = 0;

	//XXX en variable för att räkna ut det totala priset
	var totalPrice = 0;


	//XXX när http-anropet är klart körs denna callback
	function gotPizzaMenu(state, result) {
		if (state == 'done') {
			logg('gotPizzaMenu(), count = '+result.length);

			//XXX om det gick bra, skapar vi alla tableView-rader
			//makeTwRows(result);

			twRows = [];

			//XXX vi skapar en array som våra sektioner ska sparas i
			var twSections = [];
			// logg(result);
			//XXX vi sorterar alla poster i bokstavsordning, se sortData() för mer info
			var sortedArray = result.sort(sortData);
			// logg(sortedArray);

			//XXX denna array använder vi för at veta om vi redan skapt en sektion
			//	och vi sparar även positionen på den
			var sectionIds = [];

			//XXX går igenom all vår data och skapar sektioner
			for (var i = 0; i < sortedArray.length; i++) {
				//XXX vi kollar om vi redan gjort en sektione eller inte
				if (sectionIds[sortedArray[i].category] == undefined) {
					//XXX har inte skapat sektion för denna category än...
					//	vi sparar vilken position denna sektion får
					sectionIds[sortedArray[i].category] = twSections.length;

					//XXX här skapar vi sektionen
					var section = Ti.UI.createTableViewSection({
						headerTitle: sortedArray[i].category
					});

					//XXX lägg till sektionen i vår array
					twSections.push(section);
				}
			}
			logg('section.count = '+twSections.length);

			//XXX här skapas alla tw.rader
			for (var i = 0; i < sortedArray.length; i++) {
				var row = makeTwRow(sortedArray[i]);

				//vi kollar vilken sektione denna rad ska placeras i, vi sparade detta högre upp i koden
				var sId = sectionIds[sortedArray[i].category];
				//XXX för att lägga till rader i sektioner så används metoden add()
				twSections[sId].add(row);
			}

			//XXX vi sorterar våra sektioner i bokstavordning
			twSections.sort(sortSections);

			//XXX vi behöver få raderna i samma ordning som de ligger i tableView:n
			//	då vi behöver komma åt UI-objekt på raden

			//XXX första for-loopen går igenom alla sektioner
			for (var s = 0; s < twSections.length; s++) {
				//XXX andra for-loopen, går igenom alla rader (r) i denna sektion (s)
				for (var r = 0; r < twSections[s].rows.length; r++) {
					//vi lägger till raderna i vår twRows-array
					twRows.push(twSections[s].rows[r]);
				}
			}

			//XXX  nu sätter vi alla twSections i vår tableView, så de syns
			tableView.setData(twSections);

		} else {
			//XXX det gick inte bra, vi hämtar ut den engelska texten för den aktuella felkoden vi fick som svar
			var txt = httpFuncs.getHttpErrorText(result.code);

			//XXX alertD är en egen function som är definerad i app_functions.js
			//	det som skiljer sig från den inbyggda alert() är att vi kan sätta en egen titel
			appFuncs.alertD(txt, result.error);
		}
	}


	//XXX specialfunktion som används af sort() metioden på en array
	//	denna kollar efter proertyn headerTitle och jämför dessa:
	//	läs mer om egan sorteringsfunktionern här:
	// https://www.w3schools.com/jsref/jsref_sort.asp
	function sortSections(a, b) {
		return a.headerTitle > b.headerTitle ? 1 : -1;
	}
	//XXX denna specialare, sorterar på propertyn name som finns i vår JSON-data
	function sortData(a, b) {
		return a.name > b.name ? 1 : -1;
	}

	//XXX bland inte ihop med funktionen makeTwRows som skapar alla rader på en gång
	//	denna används då vi behöver lägga in rderan i rätt sektion
	function makeTwRow(rowdata) {
		//XXX en row är som en hållare för UI-objekt
		var row = Ti.UI.createTableViewRow({
			height: 48 //XXX sätter vi inge höjd autoanpassar den höjden till innehållet
		});

		if (OS_is_iOS) {
			//XXX denna property är mycket iOS-specifik och kan få Anadroid-appen att krascha
			//		vi sätter en av två fördefinierade färger när man klickar på en rad
			row.selectionStyle = Ti.UI.iOS.TableViewCellSelectionStyle.GRAY; //XXX .GRAY, .BLUE eller .NONE
		}

		//XXX för att vi ska kunna komma åt imageView, senare, så sätter vi egna proerties
		//	på alla tw-rader
		row.imgView = Ti.UI.createImageView({
			image: '/assets/images/pizza.png',
			left: 15,
			width: 40,
			height: 40
		});
		row.add(row.imgView);

		//XXX 'name' är en key/property i vår JSON
		row.titleLbl = Ti.UI.createLabel({
			text: rowdata.name,//'Title',
			//XXX på android måste vi sätta svart färg då vit är default
			color: fc.colors.black,
			left: 15 + 40 + 15,
			top: 2,
			font: fc.fonts[18]['bold']
		});
		row.add(row.titleLbl);

		//XXX 'ingredients' är en key/property i vår JSON
		row.subTitleLbl = Ti.UI.createLabel({
			text: rowdata.ingredients,//'Subtitle',
			left: 15 + 40 + 15,
			bottom: 2,
			font: fc.fonts[13]['normal']
		});
		row.add(row.subTitleLbl);

		//XXX 'price' är en key/property i vår JSON
		row.priceLbl = Ti.UI.createLabel({
			text: rowdata.price + ' kr', //XXX vi formaterar priset med kr efter
			price: rowdata.price, //XXX vi sparar även priset som en siffra, så vi senare kan lägga ihop total kostnaden
			right: 15,
			font: fc.fonts[15]['italic']
		});
		row.add(row.priceLbl);

		//XXX vi returnerar raden vi skapat, så vi kan placera den i rätt sektion sen
		return row;
	}

	function makeTwRows(data) {
		//XXX vi tar först bort ev rader som skapats tidigare
		twRows = [];

		//XXX vi loopar igenom alla poster i vår JSON (data)
		for (var i = 0; i < data.length; i++) {

			//XXX en row är som en hållare för UI-objekt
			var row = Ti.UI.createTableViewRow({
				height: 48 //XXX sätter vi inge höjd autoanpassar den höjden till innehållet
			});

			if (OS_is_iOS) {
				//XXX denna property är mycket iOS-specifik och kan få Anadroid-appen att krascha
				//		vi sätter en av två fördefinierade färger när man klickar på en rad
				row.selectionStyle = Ti.UI.iOS.TableViewCellSelectionStyle.GRAY; //XXX .GRAY, .BLUE eller .NONE
			}

			//XXX för att vi ska kunan komma åt imageView, senare, så sätter vi egna proerties
			//	på alla tw-rader
			row.imgView = Ti.UI.createImageView({
				image: '/assets/images/pizza.png',
				left: 15,
				width: 40,
				height: 40
			});
			row.add(row.imgView);

			//XXX 'name' är en key/property i vår JSON
			row.titleLbl = Ti.UI.createLabel({
				text: data[i].name,//'Title',
				left: 15 + 40 + 15,
				top: 2,
				font: fc.fonts[18]['bold']
			});
			row.add(row.titleLbl);

			//XXX 'ingredients' är en key/property i vår JSON
			row.subTitleLbl = Ti.UI.createLabel({
				text: data[i].ingredients,//'Subtitle',
				left: 15 + 40 + 15,
				bottom: 2,
				font: fc.fonts[13]['normal']
			});
			row.add(row.subTitleLbl);

			//XXX 'price' är en key/property i vår JSON
			row.priceLbl = Ti.UI.createLabel({
				text: data[i].price + ' kr', //XXX vi formaterar priset med kr efter
				price: data[i].price, //XXX vi sparar även priset som en siffra, så vi senare kan lägga ihop total kostnaden
				right: 15,
				font: fc.fonts[15]['italic']
			});
			row.add(row.priceLbl);

			//XXX raden vi precis skapat lägger vi till sist i twRows-arrayen
			twRows.push(row);
		}

		//XXX när alla rader skapats, så lägger vi till raderna i vårt tableView, via data-proertyn
		tableView.data = twRows;
	}

	var win = Ti.UI.createWindow({
		title: title,
		backgroundColor: fc.colors.winBg
	});

	var winHolder = Ti.UI.createView({
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		layout: 'composite' //vertical, horizontal, composite
	});
	win.add(winHolder);

	var dropDownHolder = Ti.UI.createView({
		height: 50,
		top: -50,
		width: '100%',
		backgroundColor: '#eee',
		zIndex: 1
	});
	winHolder.add(dropDownHolder);

	var totalPriceLbl = Ti.UI.createLabel({
		text: 'Total price: 0 kr',
		color: fc.colors.black,
		font: fc.fonts[17]['normal'],
		right: 15
	});
	dropDownHolder.add(totalPriceLbl);

	//XXX data-strukturen för den enklaste formen av en tableView (med bara en label)
	var tableData = [
		{
			title: 'Apples',
			color: '#f00',
			hasChild: true,
			header: 'Fruit'
		}, {
			title: 'Bananas',
			hasDetail: true
		}, {
			title: 'Carrots',
			hasCheck: true,
			header: "Vegetables"
		}, {
			title: 'Potatoes',
			hasCheck: false
		}
	];

	//XXX, vi testade att sätta 10000 rader i tableView och den kraschade inte
	for (var i = 0; i < 10; i++) { //XXX ändra värdet 10 till ett högre om ni vill prova själva
		tableData.push({
			className: 'myrow',
			title: 'Potatoes #' + i,
			hasCheck: false
		});
	}

	//XXX, om vi vill ha den enkla tableView:n, så sätter vi data redan här.
	var tableView = Ti.UI.createTableView({
		// data: tableData,
		top: 0,
		tableSeparatorInsets: {
			left: 15,
			right: 15
		}
	});
	winHolder.add(tableView);

	//XXX när vi vill lyssna på klick på raderna, så sätter vi lyssnaren på tableView:n
	//	och inte på raden
	tableView.addEventListener('click', function(e) {

		logg('e = ', e, true);

		//XXX vi kan direkt komma åt raden i e.row
		e.row.hasCheck = !e.row.hasCheck;

		//XXX här räknar vi hur många rader som är förkryssade, så vi kan animera totalSumman
		if (e.row.hasCheck) {
			checkedRows += 1;
		} else {
			checkedRows -= 1;
		}

		//XXX kod för att komma åt våra UI-objekt på raden
		// twRows[e.index].priceLbl.top = e.row.hasCheck ? 0 : undefined;
		// twRows[e.index].priceLbl.right = e.row.hasCheck ? 0 : 15;
		logg(twRows);

		logg('e.index = '+e.index+', e.section = '+e.section);

		//XXX här gömmer vi priset, när vi klickar på raden
		twRows[e.index].priceLbl.visible = e.row.hasCheck ? false : true;

		//XXX vi räknar ihop totalSumman för de förkryssade pizzorna
		if (e.row.hasCheck) {
			totalPrice += twRows[e.index].priceLbl.price;
		} else {
			totalPrice -= twRows[e.index].priceLbl.price;
		}
		logg('totalPrice = '+totalPrice);

		//XXX vi sätter totala priset
		totalPriceLbl.text = 'Total price: '+totalPrice+' kr';

		//XXX här animerar hållaren där vi visar totalsumman,
		//		hållaren ligger från början på top: -50 och vi animerar den ner till 0
		dropDownHolder.animate({
			top: checkedRows > 0 ? 0 : -50,
			duration: 350
		});

		//XXX samtidigt animerar vi även tableView:n, så inte vår totalSumme-hållare döljer översta raden
		//		dessa två animeringar körs samtidigt
		tableView.animate({
			top: checkedRows > 0 ? 50 : 0,
			duration: 350
		});
	});

	httpFuncs.loadJSON('http://www.woodesoft.com/utbildning/pizzameny.json', gotPizzaMenu);

	return win;
};

module.exports = ApplicationWindow;
