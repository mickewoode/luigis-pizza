function ApplicationTabGroup() {

	/* XXX
	 * 	Här skapas tabGroupen som kommer hantera vår fönster-navigering
	 * 		Den behöver normalt inte ha några properties
	 * 		Vill man kan man sätta färg (tintColor) på ikonerna så det passar med
	 * 		appens temafärger, man kan även sätta bakgrundsfärg och lite annat
	 */
	var tabGroup = Ti.UI.createTabGroup();

	/* XXX
	 * 	här initierar vi alla fönster-moduler och sparar i variablar
	 */
	var WelcomeWindow = require('WelcomeWindow');
	var MenuWindow = require('MenuWindow');
	var MapWindow = require('MapWindow');
	var NewsWindow = require('NewsWindow');

	//XXX här skapar vi alla fönster vi behöver för de fyra tabbarna
	var win1 = new WelcomeWindow(L('home')),
		win2 = new MenuWindow(L('menu')),
		win3 = new MapWindow(L('win3')),
		win4 = new NewsWindow(L('win4'));

	/* XXX
	 	en tabb skapas med en 'title', en 'icon' och med vilket fönster tabben ska kopplas
	 	ihop med 'window'

	 	titeln och ikonen visas nere på själva tabben, fönstrets titel kan vara en annan
	 	(men bör vara samma, alternativt kortare på tabben)
	*/
	var tab1 = Ti.UI.createTab({
		title: L('home'),
		icon: '/images/KS_nav_ui.png',
		window: win1
	});
	/* XXX
	 * 	'containingTab' är en egen definerad property för att fönstret senare ska kunna
	 * 		öppna nya fönster "via tabben" (med bakåt-knapp på iOS)
	 */
	win1.containingTab = tab1;

	var tab2 = Ti.UI.createTab({
		title: L('menu'),
		icon: '/images/KS_nav_views.png',
		window: win2
	});
	win2.containingTab = tab2;

	var tab3 = Ti.UI.createTab({
		title: L('win3'),
		icon: '/images/KS_nav_views.png',
		window: win3
	});
	win3.containingTab = tab3;

	var tab4 = Ti.UI.createTab({
		title: L('win4'),
		icon: '/images/KS_nav_views.png',
		window: win4
	});
	win4.containingTab = tab4;

	/* XXX
	 * 	För att tabbarna ska synas lägger vi till de till tabGroupen
	 * 		OBS, varje fönster öppnas automatiskt och ska inte öpnnas direkt av oss!
	 * 			detta sköter tabGroupen om
	 */
	tabGroup.addTab(tab1);
	tabGroup.addTab(tab2);
	tabGroup.addTab(tab3);
	tabGroup.addTab(tab4);


	/* XXX kod för att sätta vilken tab som ska vara aktiv från start
	 */
	// tabGroup.setActiveTab(tab3);

	return tabGroup;
};

module.exports = ApplicationTabGroup;
