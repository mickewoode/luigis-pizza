function ApplicationWindow(title) {

	var httpFuncs = require('http_funcs');

	var win = Ti.UI.createWindow({
		title:title,
		backgroundColor:'white'
	});

	//XXX en webView behöver en url eller html som en string
	var webView = Ti.UI.createWebView({
		//XXX propertyn html ska vara html-taggar i en sträng
		//	strängen behöver inte innehålla <html> <body> m.m, se exemplet här under
		// html: 'Normal stil <strong>fet stil</strong>',

		//XXX alternativt sätter man en länk, den kan vara extern dvs 'http://...'
		//		eller peka på en fil i Resources-mappen, tex: '/html/webpage.html'
		//		funktionen addRandomTo() ligger i http_funcs.js och lägger till en
		//		slumpad sträng bakom länken för att förhindra caching
		url: httpFuncs.addRandomTo('http://www.woodesoft.com/utbildning/marios/mario.html'),
		bottom: 50
	});
	win.add(webView);


	//XXX en webView har inget UI som användaren kanske behöver
	//	såsom en bakåt-knapp eller reload
	// så vi får skapa ett eget UI för detta
	var buttonHolder = Ti.UI.createView({
		left: 0,
		right: 0,
		height: 50,
		bottom: 0,
		backgroundColor: '#eee'
	});
	win.add(buttonHolder);

	// XXX en bakåt-knapp
	var backBtn = Ti.UI.createButton({
		title: '<<',
		// image: 'images/imageFile.png',
		left: 15,
		width: 50,
		height: 44
	});
	buttonHolder.add(backBtn);

	backBtn.addEventListener('click', function() {
		//XXX vi kör metoden goBack för att backa i historiken
		//	finns det ingen historik, händer inget
		webView.goBack();
	});

	// XXX en framåt-knapp
	var forwardBtn = Ti.UI.createButton({
		title: '>>',
		// image: 'images/imageFile.png',
		right: 15,
		width: 50,
		height: 44
	});
	buttonHolder.add(forwardBtn);

	forwardBtn.addEventListener('click', function() {
		//XXX vi kör metoden goForward för att gå framåt i historiken
		webView.goForward();
	});

	//XXX denna knapp lägger vi i navbaren på iOS,
	//	den enda propertyn som då behövs är en title eller en image
	//	resten sköter navbaren om gällande storlek och placering
	var reloadBtn = Ti.UI.createButton({
		title: 'R'
	});

	reloadBtn.addEventListener('click', function() {
		//XXX vi laddar om innehållet i webView:n med metoden reload
		webView.reload();
	});
	//XXX leftNavButton finns bara i iOS-miljö, på android syns inte knappen
	//		vill man lägga till flera kanppar eller views, använd leftNavButtons (notera 's':et)
	win.leftNavButton = reloadBtn;

	//XXX en knapp för att testa lite evalJS-grejor
	var tmpBtn = Ti.UI.createButton({
		title: '!!',
		width: 50,
		height: 44
	});
	buttonHolder.add(tmpBtn);

	tmpBtn.addEventListener('click', function() {

		//XXX vi kan köra funktioner som finns definerade i våra js-filer på webben
		// webView.evalJS('showAlert();');

		//XXX här körs funktionen 'wasUp()' den vill ha ett html objekt (som en sträng) och ett färgnamn/hex
		//		notera de olika citat-fnuttarna ' & "
		// webView.evalJS('wasUp("h1", "blue")');

		//XXX vi kan hämta ut enkla värden (String, Number, Bool) direkt från webView
		//		jag har definerat en variabel som heter globalVariable i min js-fil på servern
		var returnedVal = webView.evalJS('globalVariable');
		logg(returnedVal);

		//XXX är det mer komplex data-struktur, så måste den först göras om till en sträng
		//		därav JSON.stringify()
		var returnedVal2 = webView.evalJS('JSON.stringify(theData);');

		//XXX typeof kan användas för att få ut vilken data-typ vi har och göra med
		//		i detta fall returneras 'string'
		logg('returnedVal2 = '+returnedVal2+' is of type = '+ (typeof returnedVal2));

		//XXX för att i ska kunna ahantera vår array från servern som en array måste vi köra JSON.parse på den
		var asArray = JSON.parse(returnedVal2);
		//XXX sen beter den sig som en array igen
		logg('asArray.length = '+asArray.length);

		//XXX vi hämtar ut ett Number från variablen 'number'
		var returnedVal = webView.evalJS('number');
		logg(returnedVal);

		//XXX vi hämtar ut en Array med Numbers från variablen 'numberArr',
		//		även här måste vi göra om det till en sträng först
		var returnedVal = webView.evalJS('JSON.stringify(numberArr);');
		logg(returnedVal);

	});


	//XXX det går nästan att lägga vilket UI-objeket som helst i navbaren
	//		dock är det rekomenderat att bara lägga knappliknande objekt
	// var switchView = Ti.UI.createSwitch();
	// win.rightNavButton = switchView;



	return win;
};

module.exports = ApplicationWindow;
